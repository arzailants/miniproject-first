
-- Milestone 1
-- Actors table
CREATE TABLE actors (
  id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role_id INT UNSIGNED NOT NULL,
  verified ENUM('true', 'false') NOT NULL DEFAULT 'false',
  active ENUM('true', 'false') NOT NULL DEFAULT 'true',
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (role_id) REFERENCES roles(id)
);

-- Customers table
CREATE TABLE customers (
  id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  email VARCHAR(255) UNIQUE NOT NULL,
  avatar VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Roles table
CREATE TABLE roles (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  role_name VARCHAR(255) NOT NULL UNIQUE
);

-- Register Approval table
CREATE TABLE register_approval (
  id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  admin_id BIGINT UNSIGNED NOT NULL,
  super_admin_id BIGINT UNSIGNED NOT NULL,
  status VARCHAR(255) NOT NULL,
  FOREIGN KEY (admin_id) REFERENCES actors(id),
  FOREIGN KEY (super_admin_id) REFERENCES actors(id)
);

-- Milestone 2
-- Inserting roles
INSERT INTO roles (role_name) VALUES ('admin'), ('super admin');

-- Add foreign key to role_id
ALTER TABLE actors ADD FOREIGN KEY (role_id) REFERENCES roles(id);

-- Milestone 3
-- Inserting super admin
INSERT INTO actors (username, password, role_id) VALUES ('superadmin', 'password', 2);

-- Creating MySQL user
CREATE USER 'superadmin'@'0.0.0.0' IDENTIFIED BY 'password';

-- Granting privileges to MySQL user
GRANT ALL PRIVILEGES ON *.* TO 'superadmin'@'0.0.0.0';

-- Exporting database schema using mysqldump
mysqldump -u superadmin -p --databases crm_db > crm_db.sql